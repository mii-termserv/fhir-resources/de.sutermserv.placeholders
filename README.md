# Placeholder resources for unavailable CodeSystem resources

A number of resources are not available to be distributed via the SU-TermServ terminology server. Reasons are the general unavailability of the resource, the resource being proprietary, or the resource being too large to be distributed. This document provides placeholder resources in FHIR CodeSystem format to make this transparent.

This includes the following resources (listed with their canonical URLs) as of version `1.0.1`:
- `urn:ietf:bcp:13`
- `urn:ietf:bcp:47`
- `urn:iso:std:iso:3166`
- `http://fhir.de/CodeSystem/ifa/pzn` (VS: `http://fhir.de/ValueSet/ifa/pzn`)
- `http://www.whocc.no/atc`
- `urn:oid:2.16.840.1.113883.2.9.6.2.7`
- `http://www.nlm.nih.gov/research/umls/rxnorm`
- `http://hl7.org/fhir/sid/ndc`
- `http://fhir.de/CodeSystem/dimdi/icd-10-gm` (VS: `http://fhir.de/ValueSet/dimdi/icd-10-gm`)
- `http://fhir.de/CodeSystem/dimdi/ops` (VS: `http://fhir.de/ValueSet/dimdi/ops`)
- `http://fhir.de/CodeSystem/dimdi/atc` (VS: `http://fhir.de/ValueSet/dimdi/atc`)
- `http://fhir.de/ValueSet/abdata/wg14` (VS: `http://fhir.de/ValueSet/abdata/wg14`)
