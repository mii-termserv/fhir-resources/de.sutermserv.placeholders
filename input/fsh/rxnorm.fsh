CodeSystem: RxNorm
Title: "RxNorm"
Description: "RxNorm is a complex terminology for the coding of pharmaceutical products and substances for the use in the United States."
Id: rxnorm
* insert tag-unavailable
* ^url = "http://www.nlm.nih.gov/research/umls/rxnorm"
* ^version = "1.0.0"
* ^copyright = "National Library of Medicine, National Institutes of Health, U.S. Department of Health and Human Services"
* insert placeholder