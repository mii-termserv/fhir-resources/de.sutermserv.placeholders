CodeSystem: ISO3166Part1
Title: "ISO 3166-1"
Description: "ISO 3166-1 establishes codes that represent the current names of countries, dependencies, and other areas of particular geopolitical interest, on the basis of country names obtained from the United Nations."
Id: iso-3166-part-1
* insert tag-project(international, International standard terminology)
* insert tag-dataset(standard, International Standards)
* insert tag-license(https://mii-termserv.de/licenses#iso, ISO standards licenses)
* insert tag-unavailable
* ^url = "urn:iso:std:iso:3166"
* ^version = "1.0.0"
* ^copyright = "ISO maintains the copyright on the country codes, and controls its use carefully. For futher details see the ISO 3166 web page: https://www.iso.org/iso-3166-country-codes.html"
* insert placeholder