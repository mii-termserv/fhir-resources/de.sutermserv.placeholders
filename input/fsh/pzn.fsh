CodeSystem: PZN
Title: "Pharmazentralnummer (PZN)"
Description: "The Pharmazentralnummer (PZN) is a unique identifier for pharmaceutical products in Germany. It is assigned by Informationsstelle für Arzneispezialitäten – IFA GmbH. For more details, see the information on IFA's website: https://www.ifaffm.de/de/home"
Id: pzn
* insert tag-project(ifa, IFA GmbH)
* insert tag-dataset(medication-germany, Resources in use for medication in Germany)
* insert tag-license(https://mii-termserv.de/licenses#pzn, PZN license)
* insert tag-unavailable
* ^url = "http://fhir.de/CodeSystem/ifa/pzn"
* ^version = "1.0.0"
* ^copyright = "IFA GmbH"
* ^valueSet = "http://fhir.de/ValueSet/ifa/pzn"
* insert placeholder