CodeSystem: ATC_WHO
Title: "ATC-WHO"
Description: "The Anatomical Therapeutic Chemical Classification System (ATC) is a drug classification system. It is controlled by the WHO Collaborating Centre for Drug Statistics Methodology, Oslo, Norway. For more details, see the information on the WHO Collaborating Centre's website: http://www.whocc.no/atc. Access to a machine-readable version of the ATC WHO version is only available upon payment to the WHOCC; hence, ATC-WHO will not be distributed in FHIR format at this time."
Id: atc-who
* insert tag-project(who, World Health Organization)
* insert tag-dataset(who, World Health Organization)
* insert tag-license(https://mii-termserv.de/licenses#atc-who, ATC/WHO license)
* insert tag-unavailable
* ^url = "http://www.whocc.no/atc"
* ^version = "1.0.0"
* ^copyright = "ATC is developed and Copyright by the World Health Organization Collaborating Centre for Drug Statistics Methodology, Oslo, Norway."
* insert placeholder