CodeSystem: CAS_Registry
Title: "CAS Registry"
Description: "CAS REGISTRY is the most authoritative collection of disclosed chemical substance information. CAS REGISTRY covers substances identified from the scientific literature from 1957 to the present, with additional substances going back to the early 1900s. CAS REGISTRY is updated daily with thousands of new substances."
Id: cas
* insert tag-unavailable
* ^url = "urn:oid:2.16.840.1.113883.6.61"
* ^version = "1.0.0"
* ^copyright = "The CAS Registry is maintained by the Chemical Abstracts Society, a division of the American Chemical Society."
* insert placeholder