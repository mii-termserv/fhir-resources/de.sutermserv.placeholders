CodeSystem: OPS_DIMDI
Title: "OPS DIMDI"
Description: "The canonical URL for the OPS CodeSystem was changed from http://fhir.de/CodeSystem/dimdi/ops to http://fhir.de/CodeSystem/bfarm/ops. The code system will not be made available as FHIR using the old URI at this time."
Id: ops-dimdi
* insert tag-project(bfarm, Bundesinstitut für Arzneimittel und Medizinprodukte)
* insert tag-dataset(bfarm, Bundesinstitut für Arzneimittel und Medizinprodukte)
* insert tag-license(https://mii-termserv.de/licenses#bfarm, BfArM license terms)
* insert tag-unavailable
* ^url = "http://fhir.de/CodeSystem/dimdi/ops"
* ^valueSet = "http://fhir.de/ValueSet/dimdi/ops"
* insert placeholder