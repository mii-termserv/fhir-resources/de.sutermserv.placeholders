CodeSystem: ICD_10_GM_DIMDI
Title: "ICD-10-GM DIMDI"
Description: "The canonical URL for the ICD-10-GM CodeSystem was changed from http://fhir.de/CodeSystem/dimdi/icd-10-gm to http://fhir.de/CodeSystem/bfarm/icd-10-gm. The code system will not be made available as FHIR using the old URI at this time."
Id: icd-10-gm-dimdi
* insert tag-project(bfarm, Bundesinstitut für Arzneimittel und Medizinprodukte)
* insert tag-dataset(bfarm, Bundesinstitut für Arzneimittel und Medizinprodukte)
* insert tag-license(https://mii-termserv.de/licenses#bfarm, BfArM license terms)
* insert tag-unavailable
* ^url = "http://fhir.de/CodeSystem/dimdi/icd-10-gm"
* ^valueSet = "http://fhir.de/ValueSet/dimdi/icd-10-gm"
* insert placeholder