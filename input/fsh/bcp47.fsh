CodeSystem: BCP47
Title: "BCP47 Tags for Identifying Languages and Matching of Language Tags"
Description: "BCP13 is an internet standard for assigning language tags. It is comprised of two RFCs, RFC 4647 and RFC 5646. Language tag registration is carried out by the Internet Assigned Numbers Authority (IANA) For more details, see the the BCP47 document: https://www.rfc-editor.org/info/bcp47."
Id: bcp47
* insert tag-project(international, International standard terminology)
* insert tag-dataset(standard, International Standards)
* insert tag-license(https://mii-termserv.de/licenses#ietf-iana, IETF/IANA license terms)
* insert tag-unavailable
* ^url = "urn:ietf:bcp:47"
* ^version = "1.0.0"
* ^copyright = "IANA maintains the Language Subtag Registry. They may be freely used, see https://www.iana.org/help/licensing-terms for more information."
* insert placeholder