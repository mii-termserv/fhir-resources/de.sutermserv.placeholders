CodeSystem: ABDATA_Wirkstoffgruppe14
Title: "ABDATA Wirkstoffgruppe 14"
Description: "Die sogenannte WG14-Nummer (Wirkstoffgruppe 2014) wird von ABDATA neben der eigentlichen Standardverordnungszeile zum Zweck der eindeutigen Identifizierung erstellt und besteht aus fünf Ziffern sowie einer Prüfziffer an der sechsten Stelle. Jeder Code entspricht genau einer bestimmten Standardverordnungszeile. Über Eingabe dieses Codes in die Apotheken-Software erhält man unmittelbar eine PZN-basierte Liste aller dazugehörigen Fertigarzneimittel. Innerhalb der Zeile folgen die Wirkstoffbezeichnung, Stärke, Darreichungsform, Mengenangabe sowie Normgröße N1, N2 oder N3 – sofern vorhanden"
Id: abdata-wg14
* insert tag-dataset(medication-germany, Resources in use for medication in Germany)
* insert tag-unavailable
* ^url = "http://fhir.de/CodeSystem/abdata/wg14"
* ^version = "1.0.0"
* ^copyright = "ABDATA Pharma-Daten-Service der Avoxa - Mediengruppe Deutscher Apotheker GmbH"
* ^valueSet = "http://fhir.de/ValueSet/abdata/wg14"
* insert placeholder