Alias: $sutermserv_project = https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-project
Alias: $sutermserv_dataset = https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-dataset
Alias: $sutermserv_license = https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-license

RuleSet: tag-project(code, display)
* ^meta.tag[+].system = $sutermserv_project
* ^meta.tag[=].code = #{code}
* ^meta.tag[=].display = "{display}"

RuleSet: tag-dataset(code, display)
* ^meta.tag[+].system = $sutermserv_dataset
* ^meta.tag[=].code = #{code}
* ^meta.tag[=].display = "{display}"

RuleSet: tag-license(code, display)
* ^meta.tag[+].system = $sutermserv_license
* ^meta.tag[=].code = #{code}
* ^meta.tag[=].display = "{display}"

RuleSet: tag-unavailable
* insert tag-project(unavailable, Unavailable)
* insert tag-dataset(unavailable, Unavailable)
* insert tag-license(https://mii-termserv.de/licenses#unavailable, Unavailable)

RuleSet: placeholder
* ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/cqf-scope"
* ^extension[=].valueString = "@mii-termserv/de.sutermserv.placeholders"
* ^content = #not-present
* ^status = #active
* ^experimental = false