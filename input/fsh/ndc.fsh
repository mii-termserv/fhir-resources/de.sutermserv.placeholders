CodeSystem: NDC
Title: "NDC"
Description: "The National Drug Codes are the universal identifier for the product identification of human drugs in the United States. It is maintained by the US Food and Drug Administration (FDA)"
Id: ndc
* insert tag-unavailable
* ^url = "http://hl7.org/fhir/sid/ndc"
* ^version = "1.0.0"
* ^copyright = "United States Food and Drug Administration (FDA)"
* insert placeholder