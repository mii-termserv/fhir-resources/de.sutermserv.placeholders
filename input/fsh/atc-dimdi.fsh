CodeSystem: ATC_DIMDI
Title: "ATC DIMDI"
Description: "The canonical URL for the ATC CodeSystem was changed from http://fhir.de/CodeSystem/dimdi/atc to http://fhir.de/CodeSystem/bfarm/atc. The code system will not be made available as FHIR using the old URI at this time."
Id: atc-dimdi
* insert tag-project(bfarm, Bundesinstitut für Arzneimittel und Medizinprodukte)
* insert tag-dataset(bfarm, Bundesinstitut für Arzneimittel und Medizinprodukte)
* insert tag-license(https://mii-termserv.de/licenses#bfarm, BfArM license terms)
* insert tag-unavailable
* ^url = "http://fhir.de/CodeSystem/dimdi/atc"
* ^valueSet = "http://fhir.de/ValueSet/dimdi/atc"
* insert placeholder