CodeSystem: HGVS_Nomenclature
Title: "HGVS Nomenclature"
Description: "The HGVS Nomenclature is an internationally-recognized standard for the description of DNA, RNA, and protein sequence variants. It is used to convey variants in clinical reports and to share variants in publications and databases."
Id: hgvs-nomenclature
* insert tag-project(international, International standard terminology)
* insert tag-dataset(standard, International Standards)
* insert tag-unavailable
* ^url = "http://varnomen.hgvs.org"
* ^version = "21.0.2"
* insert placeholder