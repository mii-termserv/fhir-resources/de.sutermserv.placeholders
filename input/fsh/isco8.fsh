CodeSystem: ISCO_08
Title: "Internation Standard Classification of Occupations"
Description: "The International Standard Classification of Occupations (ISCO) is a classification of occupations maintained by the International Labour Organization (ILO). It is used to classify jobs and occupations in a standardized way. The ISCO-08 is the current version of the classification. For more details, see the information on the ILO's website: https://ilostat.ilo.org/methods/concepts-and-definitions/classification-occupation/"
Id: isco-08
* insert tag-project(international, International standard terminology)
* insert tag-dataset(standard, International Standards)
* insert tag-unavailable
* ^url = "urn:oid:2.16.840.1.113883.2.9.6.2.7"
* ^version = "1.0.0"
* ^copyright = "International Labour Organization (ILO)"
* insert placeholder