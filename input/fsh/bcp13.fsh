CodeSystem: BCP13
Title: "BCP13 Multipurpose Internet Mail Extensions (MIME) types"
Description: "BCP13 is an internet standard for the registration of Multipurpose Internet Mail Extensions (MIME) types. It specifies a registration process for obtaining MIME type. This registration is carried out by the Internet Assigned Numbers Authority (IANA). Since MIME types are inherently compositional, a listing of all MIME types is not possible. For further details see the IANA MIME Media Types web page: https://www.iana.org/assignments/media-types/media-types.xhtml and the BCP13 document: https://www.rfc-editor.org/info/bcp13."
Id: bcp13
* insert tag-project(international, International standard terminology)
* insert tag-dataset(standard, International Standards)
* insert tag-license(https://mii-termserv.de/licenses#ietf-iana, IETF/IANA license terms)
* insert tag-unavailable
* ^url = "urn:ietf:bcp:13"
* ^version = "1.0.0"
* ^copyright = "The registry of assigned MIME types is maintained by the Internet Assigned Numbers Authority (IANA). For further details see the IANA MIME Media Types web page: https://www.iana.org/assignments/media-types/media-types.xhtml and the licensing information at https://www.iana.org/help/licensing-terms"
* insert placeholder